import Vue from 'vue'
import App from './App.vue';
import VueRouter from 'vue-router';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false;
Vue.use(VueRouter);

import ResultPage from './components/ResultPage';
import TriviaGame from './components/TriviaGame';
import StartPage from './components/StartPage';

const router = new VueRouter({
  routes: [
    { path: '/', component: StartPage, name: 'StartPage' },
    { path: '/trivia', component: TriviaGame, name: 'TriviaGame' },
    { path: '/result', component: ResultPage, name: 'ResultPage' }
  ]
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
