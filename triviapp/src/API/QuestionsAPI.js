const url = 'https://opentdb.com/api.php?amount=10';

export const getQuestions = () => {
    return fetch(url)
        .then(response => response.json())
        .then(data => data.results)
    };
